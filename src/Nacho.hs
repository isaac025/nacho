module Nacho where


data NachoCommand = Put
                  | New
                  | Newcol
                  | Del
                  | Delcol
                  | Load
                  | Zerox
