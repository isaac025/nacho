{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
module Main where

import qualified Data.Text.IO as T 
import Brick.AttrMap
import Brick.Widgets.Core
import Brick.Types
import Brick.Main
import Graphics.Vty.Input.Events
import Graphics.Vty (defAttr)
import Cursor.TextField
import Cursor.Brick
import Cursor.Types
import Path.IO
import Path
import Lens.Micro.TH (makeLenses)
import Lens.Micro.Mtl
import Data.Maybe (fromMaybe)

-- State has Mode (Insert or Modal)
data TuiState = TuiState { _cur :: TextFieldCursor }
    deriving (Show, Eq)

makeLenses ''TuiState

data Resource = Resource deriving (Show, Eq, Ord)

buildInitialState :: String -> IO TuiState
buildInitialState fileResource = do
    p <- resolveFile' fileResource 
    t <- forgivingAbsence $ T.readFile (fromAbsFile p)
    let contents = fromMaybe "" t
    let tfc = makeTextFieldCursor contents
    pure TuiState { _cur = tfc } 
      
tuiApp :: App TuiState e Resource
tuiApp = App { appDraw = drawUi
             , appChooseCursor = showFirstCursor
             , appHandleEvent = handleTuiEvent 
             , appStartEvent = pure ()
             , appAttrMap = const $ attrMap defAttr [] 
             }

mDo f = do tfc <- use cur
           let tfc' = fromMaybe tfc $ f tfc
           cur .= tfc'

mDo' f = do tfc <- use cur
            let tfc' = f tfc
            cur .= tfc'

handleTuiEvent :: BrickEvent Resource e -> EventM Resource TuiState ()
handleTuiEvent (VtyEvent (EvKey KEsc [])) = halt
handleTuiEvent (VtyEvent (EvKey KRight [])) = mDo textFieldCursorSelectNextChar 
handleTuiEvent (VtyEvent (EvKey KLeft [])) = mDo textFieldCursorSelectPrevChar 
handleTuiEvent (VtyEvent (EvKey KDown [])) = mDo textFieldCursorSelectNextLine  
handleTuiEvent (VtyEvent (EvKey KUp [])) = mDo textFieldCursorSelectPrevLine  
handleTuiEvent (VtyEvent (EvKey KBS [])) = mDo $ dullMDelete . textFieldCursorRemove 
handleTuiEvent (VtyEvent (EvKey KDel [])) = mDo $ dullMDelete . textFieldCursorDelete  
handleTuiEvent (VtyEvent (EvKey KEnter [])) = mDo $ Just . textFieldCursorInsertNewline . Just 
handleTuiEvent (VtyEvent (EvKey (KChar c) [])) = mDo $ textFieldCursorInsertChar c . Just
handleTuiEvent _ = pure ()

drawUi :: TuiState -> [Widget Resource]
drawUi ts = [selectedTextFieldCursorWidget Resource (_cur ts)]  

tui :: IO ()
tui = do
    initialState <- buildInitialState "README.md"
    endState <- defaultMain tuiApp initialState
    print endState

main :: IO ()
main = tui
