# Nacho: Text Editor
Nacho is like a text editor that tries to be Acme and Vim.
The

## Approaches
1. Engineering
2. Math

## High Level
### Resources
- [x] Software Abstractions: Logic, Language, and Analysis* (SALLA)
- [ ] Parallel & Concurrent Programming in Haskell* (PCPH)
- [ ] Structural Regular Expressions

### Features
1. Structural Regular Expressions
2. DS Ropes using inside them Structural Regular Expressions (!)
3. Exec Haskell code on buffer text with mouse
4. SDL graphics library 

### ToDo 
- [ ] Read Books
- [ ] Understand Domain
- [ ] Define features/requirements
- [ ] Develop Algebra
- [ ] Develop Program Topology (Structure)
- [ ] Develop written model
- [ ] Develop alloy model
- [ ] Write tests
- [ ] Code

## Low Level
### ToDo
- [x] Finish Tom Sydney Kerckhove: Writing a text editor in Haskell with Brick
- [ ] Fix, find solution to StrWrap
- [ ] Design Windows
    - [ ] Create border/tag text window
    - [ ] add button to move window
    - [ ] order by column & window (columns have windows)
    - [ ] Windows & Columns have tags (either ID or path)
